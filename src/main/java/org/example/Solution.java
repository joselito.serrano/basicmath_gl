package org.example;

import java.util.List;
import java.text.DecimalFormat;
import java.util.ArrayList;
public class Solution {
    public static Object basicmath(String word) {
        try {
            word = word.replaceAll("\\s", "").toLowerCase();

            List<String> inputs = new ArrayList<>();

            for (String input : word.split("plus|minus|times|dividedby|modulo|\\+|-|/|\\*")) {
                inputs.add(input);
            }

            inputs.add(word.replaceAll("[\\d.]", ""));

            double firstNumber = Double.parseDouble(inputs.get(0));
            double secondNumber = Double.parseDouble(inputs.get(1));
            String operator = inputs.get(2);

            DecimalFormat df = new DecimalFormat("#.##");

            if (operator.equalsIgnoreCase("plus") || operator.equalsIgnoreCase("+")) {
                return df.format(firstNumber + secondNumber);
            } else if (operator.equalsIgnoreCase("minus") || operator.equalsIgnoreCase("-")) {
                return df.format(firstNumber - secondNumber);
            } else if (operator.equalsIgnoreCase("times") || operator.equalsIgnoreCase("*")) {
                return df.format(firstNumber * secondNumber);
            } else {
                return df.format(firstNumber / secondNumber);
            }
        } catch (Exception e) {
            return null;
        }

    }
}
