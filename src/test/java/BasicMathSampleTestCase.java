
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.example.Solution;
import org.junit.Test;
import java.text.DecimalFormat;

public class BasicMathSampleTestCase{
    DecimalFormat format = new DecimalFormat("#.##");
    @Test
    public void test___SE___Subtraction() {
        assertEquals(Solution.basicmath("10 minus 11 "),format.format(-1));
    }
    @Test
    public void test___SE___Addition() {
        assertEquals(Solution.basicmath("19 plus 12"),format.format(31));
    }
    @Test
    public void test___SE___Multiplication() {
        assertEquals(Solution.basicmath("9 times 15"),format.format(135));
    }
    @Test
    public void test___SE___Division() {
        assertEquals(Solution.basicmath("100 divided by 23.1"),format.format(4.33));
    }
    @Test
    public void test___Mod___Rand() {
        assertNull(Solution.basicmath("100 pluss 0"));
    }

    @Test
    public void test___Mod___Null() {
        assertNull(Solution.basicmath("100 di321vided by 0"));
    }
}